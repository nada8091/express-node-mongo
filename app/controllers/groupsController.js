const kcAdminClient = require("../config/keycloak");
const express = require('express');
const router = express.Router();
const keycloak = require('../config/connect.js').getKeycloak();
const grantAccess = require('../controllers/securityController').grantAccess;



router.get('/',grantAccess('readAny','teams'), function (req, res, next) {

    const users = kcAdminClient.groups.find({realm:'test'})
 
    .then(data => { res.send(JSON.stringify(data))})
    .catch(function (err) {
        res.send(err);
      });
    
     
   
  });


  router.post('/',grantAccess('createAny','teams'), function (req, res, next) {
    
    kcAdminClient.groups.create({
      realm:'test',
      name: req.body.name,
      

    }).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
     
        
  });



  router.put('/:id',grantAccess('updateAny','teams'),function (req, res, next) {


    kcAdminClient.groups.update(
      { realm: 'test',
        id: req.params.id},
      {
        name: req.body.name,
     
      },
    ).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });
  
  router.delete('/:id',grantAccess('deleteAny','teams'), function (req, res, next) {
    kcAdminClient.groups.del({
      realm: 'test',
      id: req.params.id,
    }).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });


  router.get('/:id',grantAccess('readAny','teams'), function (req, res, next) {
    kcAdminClient.groups.findOne({
      realm: 'test',
      id: req.params.id,
    }).then(data => {/*console.log(JSON.stringify(data))*/  res.send(JSON.stringify(data))})
    .catch((error) => {
      console.log(error);
     });
  });


  module.exports = router;
