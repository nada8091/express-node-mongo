const Activity = require("../models/userActivity");


// Create and Save a new activity
exports.create = (req, res) => {
   
    // Create an activity
    const activity = new Activity({
        description: req.body.description,
        username: req.body.username,
        time: req.body.time,
        browser: req.body.browser
    });

    // Save activity in the database
    activity.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while saving activity."
        });
    });
};

// Retrieve and return all activities from the database.
exports.findAll = (req, res) => {
    Activity.find()
    .then(activities => {
        res.send(activities);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving activities."
        });
    });

};

// Find a single activity with a id
exports.findOne = (req, res) => {

};

// Update an activity identified by the id in the request
exports.update = (req, res) => {

   
    // Find activity and update it with the request body
    Activity.findByIdAndUpdate(req.params.id, {
        description: req.body.description,
        username: req.body.username,
        time: req.body.time,
        browser: req.body.browser
    }, {new: true})
    .then(activity => {
        if(!activity) {
            return res.status(404).send({
                message: "Activity not found with id " + req.params.id
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "activity not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating activity with id " + req.params.id
        });
    });

};


exports.updateByUsername = (req, res) => {
   
    // Find activity and update it with the request body
    Activity.findOneAndUpdate(req.params.username, {
        description: req.body.description,
        username: req.body.username,
        lastActiveAt: req.body.lastActiveAt,
        time: req.body.time
    }, {new: true})
    .then(activity => {
        if(!activity) {
            return res.status(404).send({
                message: "Activity not found " + req.params.description
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "activity not found with id " + req.params.description
            });                
        }
        return res.status(500).send({
            message: "Error updating activity with id " + req.params.description
        });
    });

};

exports.delete = (req, res) => {

    Activity.findByIdAndRemove(req.params.id)
    .then(activity => {
        if(!activity) {
            return res.status(404).send({
                message: "Activity not found with id " + req.params.id
            });
        }
        res.send({message: "Activity deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Activity not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete activity with id " + req.params.id
        });
    });

};