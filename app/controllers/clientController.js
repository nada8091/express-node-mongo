const express = require('express');
const router = express.Router();
const keycloak = require('../config/connect.js').getKeycloak();
const kcAdminClient = require("../config/keycloak");

router.post('/confidential', function (req, res, next) {
    
    const client = kcAdminClient.clients.create({
      realm: 'test',
      clientId: req.body.clientId,
      authorizationServicesEnabled: true,
      serviceAccountsEnabled: true
      
    }).then(function (data) {
     
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
     
        
  });


  router.post('/public', function (req, res, next) {
    
    const client = kcAdminClient.clients.create({
      realm: 'test',
      clientId: req.body.clientId,
      publicClient: true,
      
      
    }).then(function (data) {
     
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
     
        
  });


module.exports = router;
