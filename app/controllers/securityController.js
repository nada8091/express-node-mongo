const { roles } = require('../config/roles');
const jwt_decode = require('jwt-decode');
const passport = require('passport');


exports.grantAccess = function (action, resource) {
    return async (req, res, next) => {
        try {

            var decoded;
            var bearer;
            var bearerToken;
            const arr = ['ROLE_IT_LEADER', 'ROLE_DEV', 'ROLE_OPS'];
            var role = [];

            /*passport.authenticate('jwt', { session: false }, (err, user, info) => {
                if (err) {
                  console.log('error is', err);
                  res.status(500).send('An error has occurred, we cannot greet you at the moment.');
                }
                else {
                  res.send({ success: true, fullName: `${user.name.givenName} ${user.name.familyName}` })
                }
            }*/

            const bearerHeader = req.headers['authorization'];
            console.log('header' + bearerHeader);
            if (bearerHeader) {
                bearer = bearerHeader.split(' ');
                bearerToken = bearer[1];
                decoded = jwt_decode(bearerToken);
                if (decoded.realm_access) {
                    console.log("decoded roles "+decoded.realm_access.roles)
                    decoded.realm_access.roles.forEach(element => {
                        if (arr.includes(element))
                            role.push(element)
                    });
                    req.token = role;

                }
                else if (decoded.groups) {
                    decoded.groups.forEach(element => {
                        if (arr.includes(element))
                            role.push(element)
                    });
                    req.token = role;

                }
                else {
                    
                        req.token = decoded.roles;
                    
                }


                //console.log(decoded);


                const Userroles = req.token;
                console.log('user roles' + Userroles);
                //console.log('req roles: ', req.user.roles,' req.action: ',action, ' req resource: ', resource);

                const permission = roles.can(Userroles)[action](resource);
                console.log(permission.granted);
                if (!permission.granted) {
                    return res.status(401).json({
                        error: "You don't have enough permission to perform this action"
                    });
                }
            }
            next()
        } catch (error) {
            next(error)
        }
    }
};


exports.allowIfLoggedin = async (req, res, next) => {
    try {
        //console.log(req.headers['authorization']);
        const user = req.headers['authorization'];
        if (!user)
            return res.status(401).json({
                error: "You need to be logged in to access this route"
            });
        req.user = user;
        next();
    } catch (error) {
        next(error);
    }
};

