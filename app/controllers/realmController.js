const express = require('express');
const router = express.Router();
const kcAdminClient = require("../config/keycloak");
const keycloak = require('../config/connect.js').getKeycloak();
const grantAccess = require('../controllers/securityController').grantAccess;




router.get('/',/*,keycloak.enforcer('realms:view') */ function (req, res, next) {
  //var token = req.kauth.grant.access_token.content;
  //var permissions = token.authorization ? token.authorization.permissions : undefined;
 // console.log.log("token + "+token)
  //console.log.log("permissions + "+permissions)

      const realms = kcAdminClient.realms.find()
      .then(data => { res.send(JSON.stringify(data))})
      .catch(function (err) {
          res.send(err);
        });
      
       
     
    });
  
  
    router.post('/',/*keycloak.enforcer('realms:create') ,*/function (req, res, next) {
      kcAdminClient.realms.create({
        realm: req.body.name,
        enabled : true
             
  
      }).then(function (data) {
        
        return res.send();
    
      }).catch((error) => {
        console.log(error);
       });
       
          
    });
  
  


    
  
    router.put('/:realm',grantAccess('updateAny','realm'),function (req, res, next) {
    
      kcAdminClient.realms.update(
        {realm: req.params.realm},
        {
         
          smtpServer: {
            auth: true,
            ssl: req.body.enableSSL,
            tls: req.body.enableTLS,
            port: req.body.port,
            from: req.body.from,
            host: req.body.host,
            user: req.body.email,
            password: req.body.password
          }
        },
      ).then(function (data) {
        console.log(data);
         
        return res.send();
    
      }).catch((error) => {
        console.log(error);
       });
      
    });
    
    router.delete('/:name',/*keycloak.enforcer('realms:delete') ,*/function (req, res, next) {
      kcAdminClient.realms.del({
        realm: req.params.name,
      }).then(function (data) {
        
        return res.send();
    
      }).catch((error) => {
        console.log(error);
       });
      
    });
  
  
    router.get('/:name', /*keycloak.enforcer('realms:viewOne'),*/function (req, res, next) {
      kcAdminClient.realms.findOne({
        realm: req.params.name,
        
      }).then(data => {console.log(JSON.stringify(data)),  res.send(JSON.stringify(data))})
      .catch((error) => {
        console.log(error);
       });
    });



module.exports = router;
