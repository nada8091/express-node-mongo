const kcAdminClient = require("../config/keycloak");
const express = require('express');
const router = express.Router();
const keycloak = require('../config/connect.js').getKeycloak();
const grantAccess = require('../controllers/securityController').grantAccess;

router.get('/me',function (req, res, next) {

  
    const whoAmI =  kcAdminClient.whoAmI.find({realm:"test"})
    .then(data => { res.send(JSON.stringify(data))})
    .catch(function (err) {
        res.send(err);
      });
    
        
  });


router.get('/',grantAccess('readAny','users'),function (req, res, next) {
  // var token = req.kauth.grant.access_token.content;
  //var permissions = token.authorization ? token.authorization.permissions : undefined;
 //console.log("token + "+token)
 // console.log("permissions + "+permissions)
 

    const users = kcAdminClient.users.find({
      realm: 'test'
    })
    .then(data => { res.send(JSON.stringify(data))})
    .catch(function (err) {
        res.send(err);
      });
    
     
   
  });

  router.get('keycloak/reset',function (req, res, next) {
    //sendEmail('31a29586-6b99-4a23-a22d-792f341a3e7a');
   
        sendEmail('7ba6747a-0716-4b62-9a7c-8c49f0abbed1','test');

  }
    );


  router.post('/',grantAccess('createAny','users'),function (req, res, next) {
    
    const user = kcAdminClient.users.create({
      realm: "test",
      username: req.body.username,
      email: req.body.email,
      firstName:req.body.firstName,
      lastName: req.body.lastName,
      enabled: true,    

    }).then(function (data) {
      addToGroup (req.body.teams,data.id);
      resetPassword(data.id,req.body.password);
      req.body.roles.forEach(element => {
        console.log(element);
        console.log(data.id);
        console.log(element.id);
        addRole (element.id,data.id,element.name);

      });
      sendEmail(data.id);

      //addRole (req.body.roles[0].id,data.id,req.body.roles[0].name);
      //console.log(req.body.roles[0].name);
      return res.send();
  
    }).catch((error) => {
      console.log(error);
      next(error);
     });
     
        
  });


  router.put('/:id',grantAccess('updateAny','users'),function (req, res, next) {

    kcAdminClient.users.update(
      { realm: "test",
      id: req.params.id},
      {
      
      email: req.body.email,
      firstName:req.body.firstName,
      lastName: req.body.lastName,
      enabled: req.body.enabled
      },
    ).then(function (data) {
      console.log(req.body.teams);
      addToGroup (req.body.teams,req.params.id);

      req.body.roles.forEach(element => {
        
        addRole (element.id,req.params.id,element.name);

      });
      return res.send();
  
    }).catch((error) => {
      console.log(error);
       next(error);
     });
    
  });
  
  router.delete('/:id',grantAccess('deleteAny','users'), function (req, res, next) {
    kcAdminClient.users.del({
      realm: 'test',

      id: req.params.id,
    }).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });


  router.get('/:id',grantAccess('readOwn','users') ,function (req, res, next) {
    kcAdminClient.users.findOne({
      realm: 'test',

      id: req.params.id,
    }).
    then(data => {  res.send(JSON.stringify(data))})
    .catch((error) => {
      console.log(error);
     });
  });


  router.put('/updateProfile/:id',grantAccess('updateOwn','users'),function (req, res, next) {


    kcAdminClient.users.update(
      { realm: "test",
      id: req.params.id},
      {
      username: req.body.username,
      email: req.body.email,
      firstName:req.body.firstName,
      lastName: req.body.lastName,
      enabled: true
      },
    ).then(function (data) {
      console.log(req.params.id);
      resetPassword(req.params.id,req.body.password);

      
        return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });

 


  router.get('/:id/groups',grantAccess('readOwn','teams'),function (req, res, next) {
      
    const groups =  kcAdminClient.users.listGroups({realm: 'test',id: req.params.id}).then(data => { res.send(JSON.stringify(data))})
    .catch(function (err) {
        res.send(err);
      });
          
    });


    router.get('/:id/roles',grantAccess('readOwn','roles'),function (req, res, next) {
      
      const groups =  kcAdminClient.users.listRealmRoleMappings({realm: 'test',id: req.params.id}).then(data => { res.send(JSON.stringify(data))})
      .catch(function (err) {
          res.send(err);
        });
            
      });

       

  router.put('/config/keycloak/user/:id/role', function (req, res, next) {
        
    kcAdminClient.users.addRealmRoleMappings({
      id: req.params.id,
      realm: 'test',

      // at least id and name should appear
      roles: [
        {
          id: currentRole.id,
          name: currentRole.name,
        },
      ],
    })
    .then(function (data) {
      console.log(data.id);
      return res.send("added role");
  
    }).catch((error) => {
      console.log(error);
     });
           
  });


  
  router.put('/config/keycloak/users/groups/:id', function (req, res, next) {
    kcAdminClient.users.addToGroup({realm: 'test',
    groupId: req.body.groupId, id: req.params.id})
    .then(function (data) {
      
      return res.send("user added to group successfully");
  
    }).catch((error) => {
      console.log(error);
     });
    
  });


  async function addToGroup (groupId,id) {
    try {
      kcAdminClient.users.addToGroup({
        realm: "test",
        groupId: groupId, 
        id: id})
      .then(function (data) {
        console.log('ok');
    
      })
    } catch {
      throw new Error()
    }
  }



  async function addRole (roleId,id,name) {
    try {
      kcAdminClient.users.addRealmRoleMappings({
        realm: "test",

        id: id,
  
        // at least id and name should appear
        roles: [
          {
            id: roleId,
            name: name,
          },
        ],
      })
      .then(function (data) {
        console.log("added role");
    
      })
    } catch {
      throw new Error();
    }
  }


  router.put('/config/keycloak/user/:id/password', function (req, res, next) {
        
    kcAdminClient.users.resetPassword({
      id: req.params.id,
      credential: {
        temporary: false,
        type: 'password',
        value: req.body.password,
      },
    })
    .then(function (data) {
      console.log(data.id);
      return res.send("password reset");
  
    }).catch((error) => {
      console.log(error);
     });
           
  });
  

  async function resetPassword (userId,value,realm) {
    try {
      kcAdminClient.users.resetPassword({
        realm: "test",
        id: userId,
        credential: {
          temporary: false,
          type: 'password',
          value: value,
        },
      })
      .then(function (data) {
        console.log("password reset");
    
      })
    } catch {
      throw new Error();
    }
  }

  async function sendEmail(userId,realm){
    

      await kcAdminClient.users.executeActionsEmail({
       
        id: userId,
        realm: "test",
        actions: ["UPDATE_PASSWORD"],
      }).then(function (data) {
        console.log("email sent");
    
      }).catch((error) => {
        console.log(error);
       });

    
  }


  module.exports = router;
