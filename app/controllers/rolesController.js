const kcAdminClient = require("../config/keycloak");
const express = require('express');
const router = express.Router();



router.get('/', function (req, res, next) {
    const roles = kcAdminClient.roles.find({ realm: 'test'})
                  
    .then(data => {res.send(JSON.stringify(data))})
    .catch(function (err) {
        res.send(err);
      });
     
     
   
  });


  router.post('/', function (req, res, next) {
    
    kcAdminClient.roles.create({
      name: req.body.name,
      

    }).then(function (data) {
      
      return res.send("sent");
  
    }).catch((error) => {
      console.log(error);
     });
     
        
  });



  router.put('/:id', function (req, res, next) {


    kcAdminClient.roles.updateById(
      {id: req.params.id},
      {
        name: req.body.name,
     
      },
    ).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });
  
  router.delete('/:id', function (req, res, next) {
    kcAdminClient.roles.delById({
      id: req.params.id,
    }).then(function (data) {
      
      return res.send();
  
    }).catch((error) => {
      console.log(error);
     });
    
  });


  router.get('/:id', function (req, res, next) {
    kcAdminClient.groups.findOneById({
      id: req.params.id,
    });
  });


  module.exports = router;
