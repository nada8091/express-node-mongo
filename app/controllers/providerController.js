const express = require('express');
const router = express.Router();
const Provider = require("../models/provider");
const grantAccess = require('./securityController').grantAccess;
const allowIfLoggedIn = require('./securityController').allowIfLoggedin;
const keycloak = require('../config/connect.js').getKeycloak();


router.post('/', allowIfLoggedIn || grantAccess('createAny','provider'),function (req, res, next) {


  Provider.create(req.body).then(function (conf) {
        return res.send(conf);

  }).catch(next);
});



router.get('/'/*,grantAccess('readAny','provider')*/,function (req, res, next) {
  
    Provider.find({}).then(function (conf) {
    res.send(conf);

  }).catch(next);
});

router.get('/name',function (req, res, next) {
  Provider.find({}).select('name -_id').then(function (conf) {
    res.send(conf);

  }).catch(next);
});


router.put('/:id',grantAccess('updateAny','provider'), function (req, res, next) {
    Provider.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function (conf) {
      Provider.findOne({ _id: req.params.id }).then(function (conf) {
        res.send(conf);
      });
    });
  });
  
  router.delete('/:id',grantAccess('deleteAny','provider') ,function (req, res, next) {
    Provider.findByIdAndRemove({ _id: req.params.id }).then(function (conf) {
      res.send(conf);
    });
  });

  module.exports = router;
