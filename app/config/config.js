const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  client_id: process.env.KEYCLOAK_CLIENT_ID,
  client_secret: process.env.KEYCLOAK_CLIENT_SECRET,
  mongo_url: process.env.MONGO_URL,
  keycloak_realm: process.env.KEYCLOAK_REALM,
  keycloak_url: process.env.KEYCLOAK_URL,
  keycloak_id: process.env.KEYCLOAK_ID,
  keycloak_secret: process.env.KEYCLOAK_SECRET,
  port: process.env.PORT


};