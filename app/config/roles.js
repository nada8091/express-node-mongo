const AccessControl = require("accesscontrol");
const ac = new AccessControl();
AllEntities = ["provider", "realm", "teams", "users", "activity","roles"];
exports.roles = (function () {

    ac.grant("ROLE_DEV")
        .readOwn("users")
        .updateOwn("users")
        .readOwn("teams")
        .readAny("realm")
        

    ac.grant("ROLE_OPS")
        .extend("ROLE_DEV")
        
    ac.grant("ROLE_IT_LEADER")
        .extend("ROLE_DEV")
        .extend("ROLE_OPS")
        .updateAny(AllEntities)
        .deleteAny(AllEntities)
        .readAny(AllEntities)
        .createAny(AllEntities)

    return ac;
})();
