const KcAdminClient = require('keycloak-admin').default;
const { client_id,client_secret,keycloak_url } = require('./config');

const kcAdminClient = new KcAdminClient();

kcAdminClient.setConfig({
    baseUrl: keycloak_url || "http://localhost:8003/auth", //keycloak-service
  });
  
  const credentials = {
    grantType: 'client_credentials',
    clientId: client_id || 'admin-api'  ,
    clientSecret: client_secret || 'dedc0f2e-e375-448c-b277-382b95eebd6a',
  };
 kcAdminClient.auth(
  {
    grantType: 'client_credentials',
    clientId: client_id || 'admin-api'  ,
    clientSecret: client_secret || 'dedc0f2e-e375-448c-b277-382b95eebd6a',
  }
  
  ).catch((error) => {
   console.log(error);
  });

  setInterval(() => kcAdminClient.auth(credentials),  60 * 1000); // 58 seconds

module.exports = kcAdminClient;