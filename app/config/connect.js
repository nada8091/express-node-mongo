var session = require('express-session');
var Keycloak = require('keycloak-connect');
const { keycloak_id,keycloak_secret,keycloak_realm,keycloak_url } = require('./config');


let _keycloak;

var keycloakConfig = {
    clientId: keycloak_id || 'backend-iam',
    bearerOnly: true,
    serverUrl: keycloak_url || 'http://localhost:8003/auth',
    realm: keycloak_realm || 'test',
    credentials: {
        secret: keycloak_secret || '3a01b1c3-5af9-4e30-b92d-fd801ee0d6df'
    }
};

function initKeycloak() {
    if (_keycloak) {
        console.warn("Trying to init Keycloak again!");
        return _keycloak;
    } 
    else {
        console.log("Initializing Keycloak...");
        var memoryStore = new session.MemoryStore();
        _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
        return _keycloak;
    }
}

function getKeycloak() {
    if (!_keycloak){
        console.error('Keycloak has not been initialized. Please called init first.');
    } 
    return _keycloak;
}

module.exports = {
    initKeycloak,
    getKeycloak
};