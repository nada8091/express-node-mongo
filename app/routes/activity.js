const express = require('express');
const router = express.Router();
const activity = require('../controllers/userActivityController.js');
const grantAccess = require('../controllers/securityController').grantAccess;


router.post('/',activity.create);

router.get('/', grantAccess('readAny','activity'),activity.findAll);

router.get('/:id', grantAccess('readAny','activity') ,activity.findOne);

router.delete('/:id', grantAccess('deleteAny','activity') ,activity.delete);

module.exports = router;
