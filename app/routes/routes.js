
module.exports = app => {

app.use('/keycloak/realms',require('../controllers/realmController.js'));
//app.use('/api',require('../controllers/googleController.js'));
//app.use('/api',require('../controllers/githubController.js'));
app.use('/keycloak/users',require('../controllers/userController.js'));
app.use('/keycloak/groups',require('../controllers/groupsController.js'));
app.use('/keycloak/roles',require('../controllers/rolesController.js'));
app.use('/provider',require('../controllers/providerController.js'));
app.use('/client',require('../controllers/clientController.js'));
//app.use('/api',require('../controllers/userActivityController.js'));




};