
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ActivitySchema = new Schema({
  description: {
    type: String,
    default: "empty"
    
  },
  username: {
    type: String,
    default: "empty"
    
  },
     
  time:{
    type: Date,
    //select: false,
    default: Date.now
  },
   browser:{
     type : String,
     default: "empty"
   }
  
 });

const Activity = mongoose.model('activity', ActivitySchema);
module.exports = Activity;