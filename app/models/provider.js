
const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ProviderSchema = new Schema({
  name: {
    type: String,
  },
  client_id: {
    type: String,
    required:false,
    default:'Empty'

  },
  
  domain: {
    type: String,
    required:false,
    default:'Empty'

  },
  secret: {
    type: String,
    required:false,
    default:'Empty'

  },
  realm: {
    type: String,
    required:false,
    default:'Empty'
  },
  tenantId: {
    type: String,
    required:false,
    default:'Empty'
  },
  
});

const Provider = mongoose.model('provider', ProviderSchema);
module.exports = Provider;