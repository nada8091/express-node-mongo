export default interface UserRepresentation {
    id?: string;
    createdTimestamp?: number;
    username?: string;
    enabled?: boolean;
    totp?: boolean;
    emailVerified?: boolean;
    disableableCredentialTypes?: string[];
    notBefore?: number;
    access?: Record<string, boolean>;
}