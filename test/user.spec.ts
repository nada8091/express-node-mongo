//import kcAdminClient from '../app/config/keycloak';
//import KcAdminClient from 'keycloak-admin';
import * as chai from 'chai';
import * as faker from "faker";
import KcAdminClient from 'keycloak-admin';

const expect = chai.expect;

interface UserRepresentation {
    id?: string;
    createdTimestamp?: number;
    username?: string;
    enabled?: boolean;
    totp?: boolean;
    emailVerified?: boolean;
    disableableCredentialTypes?: string[];
    notBefore?: number;
    access?: Record<string, boolean>;
}

const credentials = {
    grantType: 'client_credentials',
    clientId: 'admin-api',
    clientSecret: 'dedc0f2e-e375-448c-b277-382b95eebd6a',
  };


  describe('Users', function () {
    let currentUser :UserRepresentation ;
    let kcAdminClient: KcAdminClient;
    this.timeout(10000);
  
  before(async () => {
    kcAdminClient = new KcAdminClient();
    kcAdminClient.setConfig({
        baseUrl: 'http://keycloak:8080/auth', //keycloak-service
      });
    await kcAdminClient.auth({
      grantType: 'client_credentials',
      clientId: 'admin-api',
      clientSecret: 'dedc0f2e-e375-448c-b277-382b95eebd6a',
    });

    // initialize user
    const username = faker.internet.userName();
    const user = await kcAdminClient.users.create({
      username,
      email: 'nada@user.com',
      // enabled required to be true in order to send actions email
      emailVerified: true,
      enabled: true,
      attributes: {
        key: 'value',
      }
    });

    expect(user.id).to.be.ok;
    currentUser = await kcAdminClient.users.findOne({id: user.id});

    
  });


after(async () => {
    const userId = currentUser.id;
    await kcAdminClient.users.del({
      id: userId,
    });

    const user = await kcAdminClient.users.findOne({
      id: userId,
    });
    expect(user).to.be.null;
  });

  it('list users', async () => {
    const users = await kcAdminClient.users.find();
    expect(users).to.be.ok;
  });

  /*it('count users', async () => {
    const numUsers = await KcAdminClient.users.count();
    // admin user + created user in before hook
    expect(numUsers).to.equal(2);
  });


  it('update single users', async () => {
    const userId = currentUser.id;
    await KcAdminClient.users.update(
      {id: userId},
      {
        firstName: 'william',
        lastName: 'chang',
        //requiredActions: [RequiredActionAlias.UPDATE_PASSWORD],
        emailVerified: true,
      },
    );

    const user = await KcAdminClient.users.findOne({
      id: userId,
    });
    expect(user).to.deep.include({
      firstName: 'william',
      lastName: 'chang',
     // requiredActions: [RequiredActionAlias.UPDATE_PASSWORD],
      emailVerified: true,
    });
  });*/

});