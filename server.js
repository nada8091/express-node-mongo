const express = require("express");
const cors = require("cors");
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
const cookieSession = require('cookie-session');
//const passport = require('passport');
//const JwtStrategy = require('passport-jwt').Strategy,
//ExtractJwt = require('passport-jwt').ExtractJwt;
//const { secret } = require('./app/config/config.js');
const securityController = require('./app/controllers/securityController');
const { mongo_url, port } = require("./app/config/config");
const mongoURL = mongo_url || 'mongodb://user:user@localhost:27019/iam'
const app = express();

const httpServer = require('http').createServer(app);
const io = require('socket.io')(httpServer, {
  cors: {origin : '*'}
});


var corsOptions = {
  origin: ["http://localhost:4200",'http://localhost:3000'],
  allowedHeaders: "Content-Type,Authorization",
  methods: ['GET', 'PUT', 'POST', 'DELETE'],
  credentials: true

};
app.use(cookieParser());
app.use(cookieSession({
  name: 'sess',
  secret: 'secret',
  httpOnly: true
}));

app.use(cors());




//app.use(passport.initialize());
//app.use(passport.session());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

mongoose.set('useFindAndModify', false);

mongoose.connect(mongoURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log("Connected to the database!");
})
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    //process.exit();
  });
mongoose.Promise = global.Promise;


const keycloak = require('./app/config/connect.js').initKeycloak();
app.use(keycloak.middleware());

console.log(mongo_url);
// simple route
app.get("/", (req, res) => {
 
  res.json({ message: "Welcome to sso application." });
});

app.get('/favicon.ico', function (req, res) {
  res.status(204);
  res.end();
});



const cookieExtractor = function(req) {
  let token = null;
  if (req && req.cookies)
  {
    token = req.cookies['auth'];
  }
  return token;
 };
 
 
 /*const opts = {
  jwtFromRequest: ExtractJwt.fromExtractors([cookieExtractor]),
  secretOrKey: SECRET#123,
 };*/
 
 
require("./app/routes/routes")(app);
app.use('/user/activity', require('./app/routes/activity'));





io.on('connection', (socket) => {
  console.log('a user connected');


  /*socket.on('login', ({ name, room }, callback) => {
    const { user, error } = addUser(socket.id, name, room)
    if (error) return callback(error)
    socket.join(user.room)
    socket.in(room).emit('notification', { title: 'Someone\'s here', description: `${user.name} just entered the room` })
    io.in(room).emit('users', getUsers(room))
    callback()
})

socket.on('sendMessage', message => {
    const user = getUser(socket.id)
    io.in(user.room).emit('message', { user: user.name, text: message });
})

socket.on("disconnect", () => {
    console.log("User disconnected");
    const user = deleteUser(socket.id)
    if (user) {
        io.in(user.room).emit('notification', { title: 'Someone just left', description: `${user.name} just left the room` })
        io.in(user.room).emit('users', getUsers(user.room))
    }
})*/

socket.on('join', function (from,room){

 socket.join(room)
 console.log(from+" joined the room: "+room);
 socket.broadcast.to(room).emit('new user joined', { source: from, payload: 'just entered the room' })

  
});

  socket.on('message', function (from,room, message){
    
  
    console.log('recieved message from',
                  from, 'message', JSON.stringify(message));
    io.in(room).emit('message', {
      payload: message,
      source: from,
     
    });
  });

  socket.on('disconnect', () => {
    console.log('a user disconnected!');
  });
});


// set port, listen for requests
const PORT = port || 8000;
httpServer.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});